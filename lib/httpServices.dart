
import 'package:auizz/model.dart';
import 'package:auizz/services.dart';
import 'package:http/http.dart';

class HttpServ {
  final String url =
      'https://raw.githubusercontent.com/teijo/iba-cocktails/master/recipes.json';
  Future <List<User>> getUsers() async {
    final parser = SearchResultsParser();
    Response response = await get(Uri.parse(url));
    if (response.statusCode == 200) {
      return parser.parseInBackground(response.body);
    } else {
      throw 'Something wrooooong';
    }
  }
}
