import 'package:auizz/httpServices.dart';
import 'package:flutter/material.dart';
import 'model.dart';
import 'ui/DescriptionPage.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    HttpServ httpService = HttpServ();
    return MaterialApp(
      home: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          title: const Text("Flutter quizz"),
        ),
        body: FutureBuilder<List<dynamic>>(
          future: httpService.getUsers(),
          builder: (BuildContext context, AsyncSnapshot snapshot) {
            if (snapshot.hasData) {
              return ListView.builder(
                  padding: const EdgeInsets.all(8),
                  itemCount: snapshot.data.length,
                  itemBuilder: (BuildContext context, int index) {
                    User currentUser = snapshot.data[index];
                    return Card(
                      elevation: 8,
                      shadowColor: Colors.green,
                      margin: const EdgeInsets.all(20),
                      child: InkWell(
                        onTap: () {
                          Navigator.of(context).push(MaterialPageRoute(
                              builder: (context) =>
                                  DescriptionPage(currentUser: currentUser)));
                        },
                        child: Column(
                          children: <Widget>[
                            ListTile(
                              title: Text(currentUser.name ?? 'no name NPC'),
                              trailing:
                              Text(currentUser.category ?? 'no category NPC'),
                            )
                          ],
                        ),
                      ),
                    );
                  });
            } else {
              return  const Center(child: CircularProgressIndicator());
            }
          },
        ),
      ),
    );
  }
}

