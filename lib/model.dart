// To parse this JSON data, do
//
//     final user = userFromJson(jsonString);

import 'dart:convert';

List<User> userFromJson(String str) => List<User>.from(json.decode(str).map((x) => User.fromJson(x)));

String userToJson(List<User> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class User {
  User({
    this.name,
    this.glass,
    this.category,
    this.ingredients,
    this.garnish,
    this.preparation,
  });

  String? name;
  String? glass;
  String? category;
  List<Ingredient>? ingredients;
  String? garnish;
  String? preparation;

  factory User.fromJson(Map<String, dynamic> json) => User(
    name: json["name"] == null ? null : json["name"],
    glass: json["glass"] == null ? null : json["glass"],
    category: json["category"] == null ? null : json["category"],
    ingredients: json["ingredients"] == null ? null : List<Ingredient>.from(json["ingredients"].map((x) => Ingredient.fromJson(x))),
    garnish: json["garnish"] == null ? null : json["garnish"],
    preparation: json["preparation"] == null ? null : json["preparation"],
  );

  Map<String, dynamic> toJson() => {
    "name": name == null ? null : name,
    "glass": glass == null ? null : glass,
    "category": category == null ? null : category,
    "ingredients": ingredients == null ? null : List<dynamic>.from(ingredients!.map((x) => x.toJson())),
    "garnish": garnish == null ? null : garnish,
    "preparation": preparation == null ? null : preparation,
  };
}

class Ingredient {
  Ingredient({
    this.unit,
    this.amount,
    this.ingredient,
  });

  String? unit;
  double? amount;
  String? ingredient;

  factory Ingredient.fromJson(Map<String, dynamic> json) => Ingredient(
    unit: json["unit"] == null ? null : json["unit"],
    amount: json["amount"] == null ? null : json["amount"].toDouble(),
    ingredient: json["ingredient"] == null ? null : json["ingredient"],
  );

  Map<String, dynamic> toJson() => {
    "unit": unit == null ? null : unit,
    "amount": amount == null ? null : amount,
    "ingredient": ingredient == null ? null : ingredient,
  };
}
