import 'dart:convert';
import 'package:auizz/model.dart';
import 'package:flutter/foundation.dart';

class SearchResultsParser {
  SearchResultsParser();

  Future<List<User>> parseInBackground(String encodedJson) async {
    return compute(parse, encodedJson);
  }

  List<User> parse(String encodedJson) {
    List decodedResponse = json.decode(encodedJson);
    List<User> users = decodedResponse.map((e) => User.fromJson(e)).toList();
    users.sort((b, a) => a.name!.compareTo(b.name!));
    return users;
  }
}
