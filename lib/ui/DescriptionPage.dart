import 'package:auizz/consts.dart';
import 'package:auizz/model.dart';
import 'package:flutter/material.dart';

class DescriptionPage extends StatelessWidget {
  const DescriptionPage({Key? key, required this.currentUser})
      : super(key: key);
  List<Widget> buildIngridients( List<Ingredient>? ingridients) {
    if(ingridients != null){
      return ingridients.map((e) => Row(children: [
        Text ('${e.ingredient}'),
        Text('${e.ingredient}'),
        Text('${e.unit}'),
      ],)).toList();
    } else {
      return [];
    }

  }
  final User currentUser;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text(currentUser.name ?? noName)),
        body: Card(
          child: Column(
            children: [
              ListTile(
                leading: Text(currentUser.name ?? noName),
                title: Text(currentUser.category ?? noCategory),
                subtitle: Text(
                    currentUser.garnish ?? noGarnish
                ),
                trailing: Text(currentUser.glass ?? noGlass),
                isThreeLine: true,
              ),
              Column(
                children: buildIngridients(currentUser.ingredients),
              )
            ],
          ),
        ));
  }
}